package com.example.testepratico.EntityClass;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity(tableName = "Usuario")
public class Usuario {

    //Primary Key
    @PrimaryKey(autoGenerate = true)
    private  int key;

    @ColumnInfo(name = "nome")
    private String nome;

    @ColumnInfo(name = "senha")
    private String senha;

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return nome.equals(usuario.nome) &&
                senha.equals(usuario.senha);
    }
}
