package com.example.testepratico.DaoClass;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.testepratico.EntityClass.Usuario;

import java.util.List;

@Dao
public interface UsuarioDAO {

    @Insert
    void insertAllData(Usuario user);

    @Query("select * from Usuario where `key`= :id")
    Usuario getById(int id);

    @Query("select * from  Usuario")
    List<Usuario> getAllUsuarios();

    @Query("select * from Usuario where `nome`= :nome and `senha` = :senha")
    Usuario findUsuario(String nome, String senha);
}
