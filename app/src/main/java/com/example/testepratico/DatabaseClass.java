package com.example.testepratico;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.testepratico.DaoClass.UsuarioDAO;
import com.example.testepratico.EntityClass.Usuario;

@Database(entities = {Usuario.class}, version = 1)
public abstract class DatabaseClass extends RoomDatabase {

    public abstract UsuarioDAO getDao();

    private static DatabaseClass instance;

    static DatabaseClass getDatabase(final Context context) {
        if (instance == null) {
            synchronized (DatabaseClass.class) {
                instance = Room.databaseBuilder(context, DatabaseClass.class, "DATABASE").allowMainThreadQueries().build();
            }
        }
        return instance;
    }


}