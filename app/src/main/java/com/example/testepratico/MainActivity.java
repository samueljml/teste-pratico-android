package com.example.testepratico;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Database;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.testepratico.DaoClass.UsuarioDAO;
import com.example.testepratico.EntityClass.Usuario;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class MainActivity extends AppCompatActivity {

    final String defaultNome = "samuel", defaultSenha = "123456";
    EditText nome, senha;
    Button btnEntrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nome = findViewById(R.id.txtInputUsuario);
        senha = findViewById(R.id.txtInputSenha);
        btnEntrar = findViewById(R.id.btnEntrar);
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDefaultData();
                findUsuarioTeste();
            }

        });
    }

    private void setDefaultData() {
        Usuario user = new Usuario();
        user.setNome(defaultNome);
        user.setSenha(defaultSenha);
        Usuario usuario = DatabaseClass.getDatabase(getApplicationContext()).getDao().findUsuario(defaultNome, defaultSenha);

        if(usuario != null && user.equals(usuario)) {
            return;
        }
        DatabaseClass.getDatabase(getApplicationContext()).getDao().insertAllData(user);
    }

    private void findUsuarioTeste() {
        String txtNome = nome.getText().toString().toLowerCase().trim();
        String txtSenha = senha.getText().toString().trim();

        if (txtNome != null && txtSenha != null) {
            Usuario user = new Usuario();

            user.setNome(txtNome);
            user.setSenha(txtSenha);

            Usuario usuario = DatabaseClass.getDatabase(getApplicationContext()).getDao().findUsuario(txtNome, txtSenha);

            if(usuario != null && usuario.equals(user)) {
                Toast.makeText(this, "Login realizado com sucesso!", Toast.LENGTH_SHORT).show();
                goToMainScreen();
                return;
            }
        }
        Toast.makeText(this, "Dados inválidos, verifique e tente novamente", Toast.LENGTH_SHORT).show();
    }

    private void login() {
        String txtNome = nome.getText().toString().toLowerCase().trim();
        String txtSenha = senha.getText().toString().trim();

        if (txtNome != null && txtSenha != null) {
            Usuario user = new Usuario();

            user.setNome(txtNome);
            user.setSenha(txtSenha);

            List<Usuario> listaUsuarios = DatabaseClass.getDatabase(getApplicationContext()).getDao().getAllUsuarios();

            for(Usuario usuario : listaUsuarios) {
                if(usuario.equals(user)) {
                    Toast.makeText(this, "Login realizado com sucesso!", Toast.LENGTH_SHORT).show();
                    goToMainScreen();
                    return;
                }
            }
        }
        Toast.makeText(this, "Dados inválidos, verifique e tente novamente", Toast.LENGTH_SHORT).show();
    }

    public void goToMainScreen(){
        startActivity(new Intent(MainActivity.this, TelaInicialActivity.class));
        finish();
    }
}